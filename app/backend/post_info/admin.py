from django.contrib import admin

from post_info.models import Street, City, State, BuildingType, BuildingStatus, Address, Organization, POI


@admin.register(Street)
class StreetAdmin(admin.ModelAdmin):
    pass


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    pass


@admin.register(State)
class StateAdmin(admin.ModelAdmin):
    pass


@admin.register(BuildingType)
class BuildingTypeAdmin(admin.ModelAdmin):
    pass


@admin.register(BuildingStatus)
class BuildingStatusAdmin(admin.ModelAdmin):
    pass


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    pass


@admin.register(Organization)
class OrganizationAdmin(admin.ModelAdmin):
    pass


@admin.register(POI)
class POIAdmin(admin.ModelAdmin):
    pass
