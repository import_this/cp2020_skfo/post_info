from django.apps import AppConfig


class PostInfoConfig(AppConfig):
    name = 'post_info'
