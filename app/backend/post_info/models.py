from django.contrib.gis.db import models


class Street(models.Model):

    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class City(models.Model):

    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class State(models.Model):

    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class BuildingType(models.Model):

    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class BuildingStatus(models.Model):

    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class Address(models.Model):

    home_number = models.CharField(max_length=64)
    post_index = models.IntegerField()
    street = models.ForeignKey(Street, on_delete=models.CASCADE)
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    number_of_floors = models.IntegerField()
    number_of_entrances = models.IntegerField()
    coords = models.PointField(geography=True)
    building_type = models.ForeignKey(BuildingType, on_delete=models.CASCADE)
    building_status = models.ForeignKey(BuildingStatus, on_delete=models.CASCADE)
    photo = models.ImageField(null=True, blank=True)

    def __str__(self):
        return f'{self.post_index}, {self.state.name}, {self.city.name}, {self.street.name}, {self.home_number}'


class Organization(models.Model):

    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    name = models.CharField(max_length=512)
    services_sector = models.CharField(max_length=512)
    work_hours_start = models.TimeField()
    work_hours_end = models.TimeField()
    phone_number = models.CharField(max_length=32)
    social_networks = models.CharField(max_length=512)

    def __str__(self):
        return self.name


class POI(models.Model):

    name = models.CharField(max_length=512)
    coords = models.PointField(geography=True)
    related_address = models.ForeignKey(Address, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.name
