from rest_framework import serializers
from drf_extra_fields.geo_fields import PointField


from post_info.models import Street, City, State, BuildingType, BuildingStatus, Address, Organization, POI


class StreetSerializer(serializers.ModelSerializer):

    class Meta:
        model = Street
        fields = ['id', 'name']


class CitySerializer(serializers.ModelSerializer):

    class Meta:
        model = City
        fields = ['id', 'name']


class StateSerializer(serializers.ModelSerializer):

    class Meta:
        model = State
        fields = ['id', 'name']


class BuildingTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = BuildingType
        fields = ['id', 'name']


class BuildingStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = BuildingStatus
        fields = ['id', 'name']


class AddressSerializer(serializers.ModelSerializer):

    coords = PointField()

    class Meta:
        model = Address
        fields = ['id', 'home_number', 'post_index', 'street', 'city', 'state', 'number_of_floors',
                  'number_of_entrances', 'coords', 'building_type', 'building_status', 'photo']


class OrganizationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Organization
        fields = ['id', 'address', 'name', 'services_sector', 'work_hours_start', 'work_hours_end', 'phone_number',
                  'social_networks']


class POISerializer(serializers.ModelSerializer):

    coords = PointField()

    class Meta:
        model = POI
        fields = ['id', 'name', 'coords', 'related_address']
