from django.urls import include, path
from rest_framework import routers

from post_info import views

router = routers.DefaultRouter()
router.register(r'streets', views.StreetViewSet)
router.register(r'cities', views.CityViewSet)
router.register(r'states', views.StateViewSet)
router.register(r'building_types', views.BuildingTypeViewSet)
router.register(r'building_statuses', views.BuildingStatusViewSet)
router.register(r'addresses', views.AddressViewSet)
router.register(r'organizations', views.OrganizationViewSet)
router.register(r'pois', views.POIViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
