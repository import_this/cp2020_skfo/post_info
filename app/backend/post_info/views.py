from rest_framework import viewsets, permissions
from rest_framework.decorators import action
from rest_framework.response import Response

from post_info.models import Street, City, State, BuildingType, BuildingStatus, Address, Organization, POI
from post_info.serializers import StreetSerializer, CitySerializer, StateSerializer, BuildingTypeSerializer, \
    BuildingStatusSerializer, AddressSerializer, OrganizationSerializer, POISerializer


class StreetViewSet(viewsets.ModelViewSet):
    queryset = Street.objects.all()
    serializer_class = StreetSerializer
    permission_classes = [permissions.AllowAny]


class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    permission_classes = [permissions.AllowAny]


class StateViewSet(viewsets.ModelViewSet):
    queryset = State.objects.all()
    serializer_class = StateSerializer
    permission_classes = [permissions.AllowAny]


class BuildingTypeViewSet(viewsets.ModelViewSet):
    queryset = BuildingType.objects.all()
    serializer_class = BuildingTypeSerializer
    permission_classes = [permissions.AllowAny]


class BuildingStatusViewSet(viewsets.ModelViewSet):
    queryset = BuildingStatus.objects.all()
    serializer_class = BuildingStatusSerializer
    permission_classes = [permissions.AllowAny]


class AddressViewSet(viewsets.ModelViewSet):
    queryset = Address.objects.all()
    serializer_class = AddressSerializer
    permission_classes = [permissions.AllowAny]

    @action(detail=True)
    def organizations(self, request, pk=None):
        address = self.get_object()
        organizations = address.organization_set.all()
        organizations_json = OrganizationSerializer(organizations, many=True)
        return Response(organizations_json.data)

    @action(detail=True)
    def pois(self, request, pk=None):
        address = self.get_object()
        pois = address.poi_set.all()
        pois_json = POISerializer(pois, many=True)
        return Response(pois_json.data)


class OrganizationViewSet(viewsets.ModelViewSet):
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer
    permission_classes = [permissions.AllowAny]


class POIViewSet(viewsets.ModelViewSet):
    queryset = POI.objects.all()
    serializer_class = POISerializer
    permission_classes = [permissions.AllowAny]
